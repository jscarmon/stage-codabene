# CodaBene internship

Made by Jade Scarmoncin, UTC student.

This is the test to apply to CodaBene.

## Exercise 1 : A data analysis

To launch it, just execute the python file :
`python3 exercise1.py`
The first three questions will be executed.

**NB : Processes are long, it is normal that the result takes time to be shown.**

For the question 2 (relevant but not tracked in the app), the second point is not included but is commented. The process was so long that I was not able to see a result.

The bonus question is also commented.

The CSV files should be put on the same folder as the rest of the files.

## Exercise 2 : A SQL Challenge


- Question 1 : presentInShopButNotTracked
- Question 2 : expiry
- Question 3 : expiryOrdered
- Question 4 : expiryOrderedSubFamily

I have not been able to execute and test my SQL requests, this is why it is on a text format.
