import csv


def getData(file):
  """
  Get the data from a CSV file
  :param file: csv file
  :return: data from the file
  """
  data = []
  with open(file) as fileContent:
    fileReader = csv.reader(fileContent, delimiter=';')
    for row in fileReader:
        data.append(row)
  return data

def presentInShopButNotTracked(productShop, productTracked):
  """
  Get the number of products found on the shop but not tracked in the app
  :param productShop: products present in the shop
  :param productTracked: products present in the app
  :return: number of references
  """
  nbTotal = 0
  indexIdShop = productShop[0].index('EAN')
  print("Processing...")
  for i in range(1, len(productShop)):
    exist = any(productShop[i][indexIdShop] in sublist for sublist in productTracked)
    if not exist and productShop[i][indexIdShop] != '':
      nbTotal += 1
  print(nbTotal, " products not tracked in the app but present in the shop assortment")
  return nbTotal


def relevantButNotTracked(productShop, productTracked):
  """
  Get the relevant products found on the shop but not tracked in the app
  :param productShop: products present in the shop
  :param productTracked: products present in the app
  :return: list of products (EAN and Reference)
  """
  productResults = []
  indexDereferenced = productShop[0].index('Date déréf.')
  indexIdShop = productShop[0].index('EAN')
  indexReference = productShop[0].index('Libellé code interne Enseigne')
  print('Processing...')
  for i in range(1, len(productShop)):
    #1st case
    if productShop[i][indexDereferenced] == "":
      exist = any(productShop[i][indexIdShop] in sublist for sublist in productTracked)
      if not exist and productShop[i][indexIdShop] != '':
        productResults.append([productShop[i][indexIdShop], productShop[i][indexReference]])
    #2nd case
    """elif not any(productShop[i][indexIdShop] in sublist for sublist in productTracked):
      idSubFamily = productShop[0].index('Libellé  Sous-Famille ')
      for j in range(len(productShop)):
        if productShop[i][idSubFamily] != productShop[j][idSubFamily] or i == j:
          continue
        else:
          exist = any(productShop[j][indexIdShop] in sublist for sublist in productTracked)
          if exist:
            productResults.append([productShop[i][indexIdShop], productShop[i][indexReference]])
            break"""
  print("Products relevant but not tracked : ", productResults)
  return productResults

def sizeRelevantButNotTracked(products):
  """
  Get the size of relevant products found on the shop but not tracked in the app
  :param products: products relevants but not found on the app
  :return: size of list
  """
  print(len(products), " relevant products but not tracked")
  return len(products)

def suggestAisle(productShop, productTracked, productRelevant):
  """
  Suggest aisles where the product could be find
  :param productShop: products present in the shop
  :param productTracked: products present in the app
  :param productRelevant: products relevant but not found
  :return:
  """
  idSubFamily = productShop[0].index('Libellé  Sous-Famille ')
  indexIdShop = productShop[0].index('EAN')
  indexAisle = productTracked[0].index('allee')
  indexReference = productTracked[0].index('reference_id')
  for i in range(len(productRelevant)):
    subFamily = [productShop[j] for j in range(len(productShop)) if productShop[j][idSubFamily] == productRelevant[i][1] and i != j and any(productShop[j][indexIdShop] in sublist for sublist in productTracked)]
    aisles = []
    for j in subFamily:
      aisles.append([productTracked[k][indexAisle] for k in range(len(productTracked)) if productTracked[k][indexReference] == productRelevant[i][indexIdShop]])
    print("Suggestion for ", productRelevant[i], ' : ', aisles)
  


def main():
  retailerData = getData("retailer extract.csv")
  shopData = getData("references initialized in shop.csv")

  presentInShopButNotTracked(retailerData, shopData)
  relevantNotTracked = relevantButNotTracked(retailerData, shopData)
  sizeRelevantButNotTracked(relevantNotTracked)
  #suggestAisle(retailerData, shopData, relevantNotTracked)
  

  
if __name__ == "__main__":
  main()